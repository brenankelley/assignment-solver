// <copyright file="App.xaml.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Client
{
    using System.Windows;
    using Controller;
    using GUI;
    using GUI.AssignmentView;
    using GUI.PreferenceView;
    using Microsoft.Practices.Unity;
    using Provider;
    using Repository;
    using Service.AssignmentSolver;

   /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var container = new UnityContainer();
            RegisterTypes(container);

            // Resolve Types
            var views = container.ResolveAll<IView>();
            container.Resolve<ViewsUpdater>()
                     .Initialize(views);

            var mainWindow = container.Resolve<MainWindow>();
            mainWindow.DataContext = container.Resolve<VMMainWindow>();

            // Load
            MainWindow = mainWindow;
            MainWindow.Show();
        }

        private void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IAssignmentSolver, AssignmentSolver>();

            container.RegisterType<TaskController>()
                     .RegisterType<WorkerController>();

            container.RegisterSingleton<ViewsUpdater>();

            container.RegisterSingleton<ITaskProvider, TaskRepository>()
                     .RegisterSingleton<IWorkerProvider, WorkerRepository>()
                     .RegisterSingleton<IWorkerPreferenceProvider, WorkerPreferenceRepository>()
                     .RegisterSingleton<IConstraintProvider, ConstraintRepository>();

            container.RegisterSingleton<ISelectionProvider, SelectionRepository>();

            container.RegisterSingleton<VMAssignment>()
                     .RegisterVariedType<IView, VMAssignment>();

            container.RegisterSingleton<VMTaskFactory>()
                     .RegisterSingleton<VMWorkerFactory>();

            container.RegisterSingleton<VMPreference>()
                     .RegisterVariedType<IView, VMPreference>();

            container.RegisterSingleton<MainWindow>()
                     .RegisterSingleton<VMMainWindow>();
        }
    }
}
