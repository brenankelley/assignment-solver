// <copyright file="PreferenceActions.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Controller.Actions
{
    using System.Collections.Immutable;
    using System.Linq;
    using Data.Domain;
    using Repository;

    public class PreferenceActions
    {
        private readonly WorkerPreferenceRepository _preferenceRepository;

        public PreferenceActions(WorkerPreferenceRepository preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public void AddOrUpdatePreference(Worker.WorkerId worker, TaskPreference preference)
        {
            if (Preference.ContainsKey(worker))
                Preference = Preference.SetItem(worker, preference);
            else
                Preference = Preference.Add(worker, preference);
        }

        public void DeletePreference(Worker.WorkerId worker)
        {
            Preference = Preference.Remove(worker);
        }

        public void DeleteTaskFromPreferences(Task.TaskId task)
        {
            foreach (var pair in Preference)
                Preference = Preference.SetItem(pair.Key, DeleteTaskFromPreference(pair.Value, task));
        }

        private static TaskPreference DeleteTaskFromPreference(TaskPreference taskPreference, Task.TaskId task)
        {
            var groupsWithDeletedTask = taskPreference.TaskPreferenceOrder
                                                      .Select(g => DeleteTaskFromPreferenceGroup(g, task));
            return taskPreference.With(groupsWithDeletedTask);
        }

        private static TaskPreferenceGroup DeleteTaskFromPreferenceGroup(TaskPreferenceGroup preferenceGroup, Task.TaskId task)
        {
            return preferenceGroup.With(preferenceGroup.Tasks.Remove(task));
        }

        private IImmutableDictionary<Worker.WorkerId, TaskPreference> Preference
        {
            get { return _preferenceRepository.WorkerPreference; }
            set { _preferenceRepository.WorkerPreference = value; }
        }
    }
}