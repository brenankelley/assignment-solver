// <copyright file="TaskActions.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Controller.Actions
{
    using System;
    using System.Collections.Immutable;
    using System.Linq;
    using Data.Domain;
    using Repository;

    public class TaskActions
    {
        private readonly TaskRepository _taskRepository;

        public TaskActions(TaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public void AddTask(Task task)
        {
            if (Tasks.Any(w => w.Id.Equals(task.Id)))
                throw new InvalidOperationException($"{task.GetType().Name} already exists");

            Tasks = Tasks.Add(task);
        }

        public void UpdateTask(Task task)
        {
            var oldWorker = Tasks.Single(w => w.Id.Equals(task.Id));
            Tasks = Tasks.Remove(oldWorker)
                         .Add(task);
        }

        public void RemoveTask(Task task)
        {
            Tasks = Tasks.Remove(task);
        }

        private IImmutableSet<Task> Tasks
        {
            get { return _taskRepository.Tasks; }
            set { _taskRepository.Tasks = value; }
        }
    }
}