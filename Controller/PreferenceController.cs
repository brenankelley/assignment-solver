// <copyright file="PreferenceController.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Controller
{
    using Actions;
    using Data.Domain;

    public class PreferenceController
    {
        private readonly PreferenceActions _preferenceActions;
        private readonly ViewsUpdater _viewsUpdater;

        public PreferenceController(PreferenceActions preferenceActions, ViewsUpdater viewsUpdater)
        {
            _preferenceActions = preferenceActions;
            _viewsUpdater = viewsUpdater;
        }

        public void AddOrUpdatePreference(Worker.WorkerId worker, TaskPreference preference)
        {
            _preferenceActions.AddOrUpdatePreference(worker, preference);
            _viewsUpdater.UpdateViews();
        }

        public void DeletePreference(Worker.WorkerId worker)
        {
            _preferenceActions.DeletePreference(worker);
            _viewsUpdater.UpdateViews();
        }
    }
}