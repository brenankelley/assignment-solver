﻿// <copyright file="TaskController.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Controller
{
    using Actions;
    using Data.Domain;

    public class TaskController
    {
        private readonly TaskActions _taskActions;
        private readonly PreferenceActions _preferenceActions;
        private readonly ViewsUpdater _viewsUpdater;

        public TaskController(TaskActions taskActions,
                              PreferenceActions preferenceActions,
                              ViewsUpdater viewsUpdater)
        {
            _taskActions = taskActions;
            _preferenceActions = preferenceActions;
            _viewsUpdater = viewsUpdater;
        }

        public void AddTask(Task task)
        {
            _taskActions.AddTask(task);
            _viewsUpdater.UpdateViews();
        }

        public void UpdateTask(Task task)
        {
            _taskActions.UpdateTask(task);
            _viewsUpdater.UpdateViews();
        }

        public void RemoveTask(Task task)
        {
            _taskActions.RemoveTask(task);
            _preferenceActions.DeleteTaskFromPreferences(task);
            _viewsUpdater.UpdateViews();
        }
    }
}