﻿// <copyright file="DifferenceCalculator.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Data.Core.Difference
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;

    public class DifferenceCalculator<T>
    {
        private IImmutableList<T> _lastList;

        public DifferenceCalculator()
        {
            _lastList = ImmutableList.Create<T>();
        }

        public Difference<T> Calculate(IEnumerable<T> nextList)
        {
            return Calculate(nextList.ToImmutableList());
        }

        private Difference<T> Calculate(IImmutableList<T> nextList)
        {
            var unchanged = _lastList.Intersect(nextList).ToList();
            var removed = _lastList.Except(unchanged);
            var added = nextList.Except(unchanged);

            var difference = new Difference<T>(added, removed);
            _lastList = nextList;
            return difference;
        }
    }
}
