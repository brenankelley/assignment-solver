﻿// <copyright file="Optional.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Data.Core
{
    using System;

    public static class Optional
    {
        public static Optional<T> With<T>(T data)
            where T : class
        {
            return new Optional<T>(data);
        }

        public static EmptyOptional Empty => default(EmptyOptional);

        public struct EmptyOptional
        {
        }
    }

#pragma warning disable SA1402 // File may only contain a single class
    public struct Optional<T>
#pragma warning restore SA1402 // File may only contain a single class
        where T : class
    {
        private readonly T _value;

        public Optional(T value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            _value = value;
        }

        public static implicit operator Optional<T>(Optional.EmptyOptional empty)
        {
            return default(Optional<T>);
        }

        public T Value
        {
            get
            {
                if (_value == null)
                    throw new InvalidOperationException("Value is Null");
                return _value;
            }
        }

        public bool HasValue => _value != null;
    }
}
