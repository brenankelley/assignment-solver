﻿// <copyright file="Task.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.Data.Domain
{
    using Core;

    public class Task
    {
        public Task(string name, int effort)
            : this(new TaskId(), name, effort)
        {
        }

        private Task(TaskId id, string name, int effort)
        {
            Id = id;
            Name = name;
            Effort = effort;
        }

        public TaskId Id { get; }
        public string Name { get; }
        public int Effort { get; }

        public static implicit operator TaskId(Task task) => task.Id;

        public Task With(string name)
        {
            return new Task(Id, name, Effort);
        }

        public Task With(int effort)
        {
            return new Task(Id, Name, effort);
        }

        public class TaskId : IdBase
        {
        }
    }
}