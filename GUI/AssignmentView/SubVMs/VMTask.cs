﻿// <copyright file="VMTask.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.AssignmentView.SubVMs
{
    using System.Linq;
    using Common.VM.VMProperty;
    using Controller;
    using Data.Domain;
    using Provider;

    public class VMTask : IVMTask
    {
        private readonly ITaskProvider _taskProvider;
        private readonly TaskController _taskController;
        private readonly Task.TaskId _id;

        public VMTask(ITaskProvider taskProvider, TaskController taskController, Task.TaskId taskid)
        {
            _taskProvider = taskProvider;
            _taskController = taskController;
            _id = taskid;

            Name = VMPropBuilder.Create(Task.Name)
                                .AddOnUserChangeCallback(UserChange)
                                .AddValidation(i => i.Any(), "Name must not be empty")
                                .Build();

            Effort = VMPropBuilder.Create<int?>(Task.Effort)
                                  .AddOnUserChangeCallback(UserChange)
                                  .AddValidation(i => i.HasValue, "Effort must not be empty")
                                  .AddValidation(i => i.HasValue,
                                                 i => i.Value >= 0,
                                                 "Effort must be a natural number")
                                  .Build();
        }

        public VMProp<string> Name { get; }
        public VMProp<int?> Effort { get; }

        private void UserChange()
        {
            if (Name.HasErrors || Effort.HasErrors)
                return;

            var task = Task.With(Name.View)
                           .With(Effort.View.Value);
            _taskController.UpdateTask(task);
        }

        public void UpdateSubVM()
        {
            Name.Model = Task.Name;
            Effort.Model = Task.Effort;
        }

        private Task Task => _taskProvider.Tasks.Single(t => t.Id.Equals(_id));
    }
}
