﻿// <copyright file="VMWorker.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.AssignmentView.SubVMs
{
    using System.Linq;
    using Common.VM.VMProperty;
    using Controller;
    using Data.Domain;
    using Provider;

    public class VMWorker : IVMWorker
    {
        private readonly IWorkerProvider _workerProvider;
        private readonly WorkerController _workerController;

        public VMWorker(IWorkerProvider workerProvider, WorkerController workerController, Worker.WorkerId id)
        {
            _workerProvider = workerProvider;
            _workerController = workerController;
            Id = id;

            Name = VMPropBuilder.Create(Worker.Name)
                                .AddOnUserChangeCallback(UserChange)
                                .AddValidation(i => i.Any(), "Name must not be empty")
                                .Build();

            Capacity = VMPropBuilder.Create<int?>(Worker.Capacity)
                                    .AddOnUserChangeCallback(UserChange)
                                    .AddValidation(i => i.HasValue, "Capacity must not be empty")
                                    .AddValidation(i => i.HasValue,
                                                   i => i.Value >= 0,
                                                   "Capacity must be a natural number")
                                    .Build();
        }

        public Worker.WorkerId Id { get; }
        public VMProp<string> Name { get; }
        public VMProp<int?> Capacity { get; }

        private void UserChange()
        {
            if (Name.HasErrors || Capacity.HasErrors)
                return;

            var worker = Worker.With(Name.View)
                               .With(Capacity.View.Value);
            _workerController.UpdateWorker(worker);
        }

        public void UpdateSubVM()
        {
            Name.Model = Worker.Name;
            Capacity.Model = Worker.Capacity;
        }

        private Worker Worker => _workerProvider.Workers.Single(t => t.Id.Equals(Id));
    }
}
