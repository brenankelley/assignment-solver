﻿// <copyright file="VMAssignment.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.AssignmentView
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Common.VM;
    using Common.VM.VMProperty;
    using Controller;
    using Data.Core.Difference;
    using Data.Domain;
    using Provider;
    using SubVMs;

    public sealed class VMAssignment : IView, IVMAssignment
    {
        private readonly VMTaskFactory _vmTaskFactory;
        private readonly VMWorkerFactory _vmWorkerFactory;
        private readonly ITaskProvider _taskProvider;
        private readonly IWorkerProvider _workerProvider;
        private readonly ISelectionProvider _selectionProvider;
        private readonly SelectionController _selectionController;
        private readonly DifferenceCalculator<Task.TaskId> _taskDiffCalculator;
        private readonly Dictionary<Task.TaskId, IVMTask> _idToVMTask;
        private readonly DifferenceCalculator<Worker.WorkerId> _workerDiffCalculator;
        private readonly Dictionary<Worker.WorkerId, IVMWorker> _idToVMWorker;

        public VMAssignment(VMTaskFactory vmTaskFactory,
                            VMWorkerFactory vmWorkerFactory,
                            VMCreateTask vmCreateTask,
                            VMCreateWorker vmCreateWorker,
                            ITaskProvider taskProvider,
                            IWorkerProvider workerProvider,
                            ISelectionProvider selectionProvider,
                            SelectionController selectionController)
        {
            _vmTaskFactory = vmTaskFactory;
            _vmWorkerFactory = vmWorkerFactory;
            _taskProvider = taskProvider;
            _workerProvider = workerProvider;
            _selectionProvider = selectionProvider;
            _selectionController = selectionController;
            _taskDiffCalculator = new DifferenceCalculator<Task.TaskId>();
            _idToVMTask = new Dictionary<Task.TaskId, IVMTask>();
            _workerDiffCalculator = new DifferenceCalculator<Worker.WorkerId>();
            _idToVMWorker = new Dictionary<Worker.WorkerId, IVMWorker>();
            VMCreateTask = vmCreateTask;
            VMCreateWorker = vmCreateWorker;

            Tasks = VMPropBuilder.Create<IEnumerable<IVMTask>>(new List<IVMTask>())
                                 .Build();
            Workers = VMPropBuilder.Create<IEnumerable<IVMWorker>>(new List<IVMWorker>())
                                   .Build();
            SelectedWorkers = VMPropBuilder.Create<IEnumerable>(new List<IVMWorker>())
                                           .AddOnUserChangeCallback(UserUpdated)
                                           .Build();
        }

        public VMProp<IEnumerable<IVMTask>> Tasks { get; }

        public VMCreateTask VMCreateTask { get; }

        public VMProp<IEnumerable<IVMWorker>> Workers { get; }

        public VMCreateWorker VMCreateWorker { get; }

        public VMProp<IEnumerable> SelectedWorkers { get; }

        private void UserUpdated()
        {
            _selectionController.SetSelection(SelectedWorkers.View.OfType<IVMWorker>().Select(vm => vm.Id));
        }

        public void Update()
        {
            Tasks.Model = GetUpdatedTaskVMs();
            Workers.Model = GetUpdatedWorkerVMs();
            SelectedWorkers.Model = _selectionProvider.Workers.Select(w => _idToVMWorker[w]).ToList();

            foreach (var updateVM in SubVMs)
                updateVM.UpdateSubVM();
        }

        private IEnumerable<ISubVM> SubVMs => Tasks.Model.Cast<ISubVM>().Concat(Workers.Model);

        private ICollection<IVMTask> GetUpdatedTaskVMs()
        {
            var taskDiff = _taskDiffCalculator.Calculate(_taskProvider.Tasks.Select(i => i.Id));
            var newTasks = Tasks.Model.ToList();

            foreach (var id in taskDiff.Removed)
            {
                var removeVM = _idToVMTask[id];
                newTasks.Remove(removeVM);
                _idToVMTask.Remove(id);
            }

            foreach (var id in taskDiff.Added)
            {
                var addVM = _vmTaskFactory.CreateVM(id);
                newTasks.Add(addVM);
                _idToVMTask[id] = addVM;
            }

            return newTasks;
        }

        private ICollection<IVMWorker> GetUpdatedWorkerVMs()
        {
            var workerDiff = _workerDiffCalculator.Calculate(_workerProvider.Workers.Select(i => i.Id));
            var newWorkers = Workers.Model.ToList();

            foreach (var id in workerDiff.Removed)
            {
                var removeVM = _idToVMWorker[id];
                newWorkers.Remove(removeVM);
                _idToVMWorker.Remove(id);
            }

            foreach (var id in workerDiff.Added)
            {
                var addVM = _vmWorkerFactory.CreateVM(id);
                newWorkers.Add(addVM);
                _idToVMWorker[id] = addVM;
            }

            return newWorkers;
        }
    }
}