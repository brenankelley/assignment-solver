﻿// <copyright file="VMAssignmentDesign.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.AssignmentView
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Common.VM.VMProperty;
    using Data.Domain;
    using SubVMs;

    public class VMAssignmentDesign : IVMAssignment
    {
        public VMAssignmentDesign()
        {
            var vmTasks = new[]
                          {
                              new VMTask("Test Task 1", 10),
                              new VMTask("Test Task 2", 15),
                              new VMTask("Task With LONNNNNNNNNNNNNNG Name", 10064),
                              new VMTask("Some more rows", 10),
                              new VMTask("Some more rows", 10),
                              new VMTask("Some more rows", 10),
                              new VMTask("Some more rows", 10),
                              new VMTask("Some more rows", 10),
                          };
            Tasks = VMPropBuilder.Create<IEnumerable<IVMTask>>(vmTasks).Build();

            var vmWorkers = new[]
                            {
                                new VMWorker("Worker Blue", 100),
                                new VMWorker("The A Worker", 9999),
                                new VMWorker("Worker Green", 123),
                                new VMWorker("Worker Dinkeldwarf", 50),
                            };
            Workers = VMPropBuilder.Create<IEnumerable<IVMWorker>>(vmWorkers).Build();
            SelectedWorkers = VMPropBuilder.Create<IEnumerable>(new[] { vmWorkers.First() }).Build();
        }

        public VMProp<IEnumerable<IVMTask>> Tasks { get; }

        public VMCreateTask VMCreateTask { get; }

        public VMProp<IEnumerable<IVMWorker>> Workers { get; }

        public VMCreateWorker VMCreateWorker { get; }

        public VMProp<IEnumerable> SelectedWorkers { get; }

        internal class VMTask : IVMTask
        {
            public VMTask(string name, int? effort)
            {
                Name = new VMProp<string>(name);
                Effort = new VMProp<int?>(effort);
            }

            public VMProp<string> Name { get; }
            public VMProp<int?> Effort { get; }

            public void UpdateSubVM()
            {
            }
        }

        internal class VMWorker : IVMWorker
        {
            public VMWorker(string name, int? capacity)
            {
                Name = new VMProp<string>(name);
                Capacity = new VMProp<int?>(capacity);
            }

            public Worker.WorkerId Id { get; }
            public VMProp<string> Name { get; }
            public VMProp<int?> Capacity { get; }

            public void UpdateSubVM()
            {
            }
        }
    }
}
