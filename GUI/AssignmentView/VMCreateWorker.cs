// <copyright file="VMCreateWorker.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.AssignmentView
{
    using System.Linq;
    using System.Windows.Input;
    using Common;
    using Common.VM.VMProperty;
    using Controller;
    using Data.Domain;

    public sealed class VMCreateWorker
    {
        private readonly WorkerController _workerController;

        public VMCreateWorker(WorkerController workerController)
        {
            _workerController = workerController;
            NewWorkerName = VMPropBuilder.Create(string.Empty)
                                         .AddValidation(i => i.Any(), "Name must not be empty")
                                         .Build();

            NewWorkerCapacity = VMPropBuilder.Create<int?>(null)
                                             .AddValidation(i => i.HasValue, "Capacity must not be empty")
                                             .AddValidation(i => i.HasValue,
                                                            i => i.Value >= 0,
                                                            "Capacity must be a natural number")
                                             .Build();
            CreateWorker = new ToggleCommand(OnCreateWorker);
        }

        public VMProp<string> NewWorkerName { get; }
        public VMProp<int?> NewWorkerCapacity { get; }
        public ICommand CreateWorker { get; }

        private void OnCreateWorker()
        {
            if (NewWorkerName.HasErrors || NewWorkerCapacity.HasErrors)
                return;

            _workerController.AddWorker(new Worker(NewWorkerName.View, NewWorkerCapacity.View.Value));
            NewWorkerName.Model = string.Empty;
            NewWorkerCapacity.Model = null;
        }
    }
}