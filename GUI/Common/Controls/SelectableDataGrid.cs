// <copyright file="SelectableDataGrid.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.Common.Controls
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;

    internal class SelectableDataGrid : DataGrid
    {
        public static readonly DependencyProperty BindableSelectableItemsProperty =
            DependencyProperty.RegisterAttached(nameof(BindableSelectableItems),
                                                typeof(IEnumerable),
                                                typeof(SelectableDataGrid),
                                                new FrameworkPropertyMetadata(Enumerable.Empty<object>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnPropertyChanged));

        private static void OnPropertyChanged(DependencyObject dependencyObject,
                                              DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = (SelectableDataGrid)dependencyObject;
            dataGrid.UpdateControl(((IEnumerable)e.NewValue).OfType<object>());
        }

        internal IEnumerable BindableSelectableItems
        {
            get { return (IEnumerable)GetValue(BindableSelectableItemsProperty); }
            set { SetValue(BindableSelectableItemsProperty, value); }
        }

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);
            BindableSelectableItems = SelectedItems;
        }

        private void UpdateControl(IEnumerable<object> selectedItems)
        {
            if (!selectedItems.Any())
                return;

            if (SelectionMode == DataGridSelectionMode.Single)
            {
                var item = selectedItems.Single();
                SelectedItem = item;
            }
            else
            {
                SelectedItems.Clear();
                foreach (var obj in selectedItems)
                    SelectedItems.Add(obj);
            }

            Focus();
        }
    }
}