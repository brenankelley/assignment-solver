// <copyright file="DropHandler.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.Common.VM
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using GongSolutions.Wpf.DragDrop;
    using GongSolutions.Wpf.DragDrop.Utilities;

    public class DropHandler : IDropTarget
    {
        private readonly bool _allowCopying;

        public DropHandler(bool allowCopying)
        {
            _allowCopying = allowCopying;
        }

        public virtual void DragOver(IDropInfo dropInfo)
        {
            if (!CanAcceptData(dropInfo))
                return;

            dropInfo.Effects = AllowCopyingData(dropInfo) ? DragDropEffects.Copy : DragDropEffects.Move;
            var isTreeViewItem = dropInfo.InsertPosition.HasFlag(RelativeInsertPosition.TargetItemCenter)
                                 && dropInfo.VisualTargetItem is TreeViewItem;
            dropInfo.DropTargetAdorner = isTreeViewItem ? DropTargetAdorners.Highlight : DropTargetAdorners.Insert;
        }

        public virtual void Drop(IDropInfo dropInfo)
        {
            if (dropInfo?.DragInfo == null)
                return;

            var insertIndex = dropInfo.InsertIndex;
            var destinationList = GetList(dropInfo.TargetCollection);
            var data = ExtractData(dropInfo.Data);

            if (!AllowCopyingData(dropInfo))
            {
                var sourceList = GetList(dropInfo.DragInfo.SourceCollection);

                foreach (var o in data)
                {
                    var index = sourceList.IndexOf(o);

                    if (index != -1)
                    {
                        sourceList.RemoveAt(index);

                        if (sourceList.Equals(destinationList) && index < insertIndex)
                            --insertIndex;
                    }
                }
            }

            foreach (var o in data)
            {
                destinationList.Insert(insertIndex++, o);
            }
        }

        private bool AllowCopyingData(IDropInfo dropInfo)
        {
            return _allowCopying
                   && !(dropInfo.DragInfo.SourceItem is HeaderedContentControl)
                   && !(dropInfo.DragInfo.SourceItem is HeaderedItemsControl)
                   && !(dropInfo.DragInfo.SourceItem is ListBoxItem);
        }

        public static bool CanAcceptData(IDropInfo dropInfo)
        {
            if (dropInfo?.DragInfo == null)
                return false;

            if (dropInfo.DragInfo.SourceCollection == dropInfo.TargetCollection)
                return GetList(dropInfo.TargetCollection) != null;

            if (dropInfo.DragInfo.SourceCollection is ItemCollection)
                return false;
            if (dropInfo.TargetCollection == null)
                return false;

            if (TestCompatibleTypes(dropInfo.TargetCollection, dropInfo.Data))
                return !IsChildOf(dropInfo.VisualTargetItem, dropInfo.DragInfo.VisualSourceItem);

            return false;
        }

        public static IEnumerable ExtractData(object data)
        {
            if (data is IEnumerable && !(data is string))
                return (IEnumerable)data;
            return Enumerable.Repeat(data, 1);
        }

        public static IList GetList(IEnumerable enumerable)
        {
            if (enumerable is ICollectionView)
                return ((ICollectionView)enumerable).SourceCollection as IList;
            return enumerable as IList;
        }

        protected static bool IsChildOf(UIElement targetItem, UIElement sourceItem)
        {
            var parent = ItemsControl.ItemsControlFromItemContainer(targetItem);

            while (parent != null)
            {
                if (parent.Equals(sourceItem))
                    return true;

                parent = ItemsControl.ItemsControlFromItemContainer(parent);
            }

            return false;
        }

        protected static bool TestCompatibleTypes(IEnumerable target, object data)
        {
            TypeFilter filter = (t, o) => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IEnumerable<>);

            var enumerableInterfaces = target.GetType().FindInterfaces(filter, null);
            var enumerableTypes = from i in enumerableInterfaces select i.GetGenericArguments().Single();

            if (!enumerableTypes.Any())
                return target is IList;

            var dataType = TypeUtilities.GetCommonBaseClass(ExtractData(data));
            return enumerableTypes.Any(t => t.IsAssignableFrom(dataType));
        }
    }
}
