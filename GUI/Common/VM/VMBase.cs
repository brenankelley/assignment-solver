// <copyright file="VMBase.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.Common.VM
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using JetBrains.Annotations;

    public abstract class VMBase : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private readonly ErrorContainer _errorContainer;
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        protected VMBase()
        {
            _errorContainer = new ErrorContainer();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void Validate(bool arePrereqsValid, Func<bool> isValid, string description, [CallerMemberName] string propertyName = null)
        {
            if (!arePrereqsValid)
            {
                _errorContainer.RemoveError(propertyName, description);
                return;
            }

            Validate(isValid(), description, propertyName);
        }

        protected virtual void Validate(bool isValid, string description, [CallerMemberName] string propertyName = null)
        {
            if (isValid)
                _errorContainer.RemoveError(propertyName, description);
            else
                _errorContainer.AddError(propertyName, description);

            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public bool HasErrors => _errorContainer.HasErrors();

        public IEnumerable GetErrors(string propertyName)
        {
            return _errorContainer.GetErrors(propertyName);
        }

        private class ErrorContainer
        {
            private readonly Dictionary<string, IImmutableSet<string>> _propertyToErrors;

            protected internal ErrorContainer()
            {
                _propertyToErrors = new Dictionary<string, IImmutableSet<string>>();
            }

            protected internal bool HasErrors()
            {
                return _propertyToErrors.Values.SelectMany(i => i).Any();
            }

            protected internal IEnumerable<string> GetErrors(string propertyName)
            {
                if (!_propertyToErrors.ContainsKey(propertyName))
                    return ImmutableList.Create<string>();

                if (!_propertyToErrors[propertyName].Any())
                    return ImmutableList.Create<string>();

                return _propertyToErrors[propertyName];
            }

            protected internal void AddError(string propertyName, string errorDescription)
            {
                if (_propertyToErrors.ContainsKey(propertyName))
                {
                    if (_propertyToErrors[propertyName].Contains(errorDescription))
                        return;
                    _propertyToErrors[propertyName] = _propertyToErrors[propertyName].Add(errorDescription);
                }

                _propertyToErrors[propertyName] = new[] { errorDescription }.ToImmutableHashSet();
            }

            protected internal void RemoveError(string propertyName, string errorDescription)
            {
                if (!_propertyToErrors.ContainsKey(propertyName))
                    return;

                if (!_propertyToErrors[propertyName].Contains(errorDescription))
                    return;

                _propertyToErrors[propertyName] = _propertyToErrors[propertyName].Remove(errorDescription);
            }
        }
    }
}