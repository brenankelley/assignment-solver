// <copyright file="VMProp.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.Common.VM.VMProperty
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using JetBrains.Annotations;

   public sealed class VMProp<T> : VMBase
    {
        private readonly IImmutableList<ValidationFunction<T>> _validators;
        private readonly Action _onUserEdited;
        private T _internal;

        public VMProp(T defaultValue)
            : this(null, Enumerable.Empty<ValidationFunction<T>>(), defaultValue)
        {
        }

        public VMProp([NotNull] Action onUserEdited, T defaultValue)
            : this(onUserEdited, Enumerable.Empty<ValidationFunction<T>>(), defaultValue)
        {
        }

        internal VMProp(Action onUserEdited, IEnumerable<ValidationFunction<T>> validators, T defaultValue)
        {
            _onUserEdited = onUserEdited;
            _validators = validators.ToImmutableList();
            Internal = defaultValue;
        }

        public T View
        {
            get { return Internal; }
            set
            {
                Internal = value;
                _onUserEdited?.Invoke();
                OnPropertyChanged();
            }
        }

        public T Model
        {
            get { return Internal; }
            set
            {
                Internal = value;
                OnPropertyChanged(nameof(View));
            }
        }

        private T Internal
        {
            get { return _internal; }
            set
            {
                _internal = value;

                foreach (var validator in _validators)
                {
                    Validate(validator.ArePrereqsValid(value),
                             () => validator.IsValid(value),
                             validator.ErrorDescription,
                             nameof(View));
                }
            }
        }

        public class ValidationFunction<TItem>
        {
            public ValidationFunction(string errorDescription, Func<TItem, bool> isValidFunction)
                : this(errorDescription, i => true, isValidFunction)
            {
            }

            public ValidationFunction(string errorDescription,
                                      Func<TItem, bool> arePrereqsValid,
                                      Func<TItem, bool> isValid)
            {
                ErrorDescription = errorDescription;
                ArePrereqsValid = arePrereqsValid;
                IsValid = isValid;
            }

            public string ErrorDescription { get; }
            public Func<TItem, bool> ArePrereqsValid { get; }
            public Func<TItem, bool> IsValid { get; }
        }
    }
}