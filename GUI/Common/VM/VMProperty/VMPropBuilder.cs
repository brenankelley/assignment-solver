// <copyright file="VMPropBuilder.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.Common.VM.VMProperty
{
    using System;
    using System.Collections.Generic;

    public class VMPropBuilder
    {
        public static VMPropBuilderFluent<T> Create<T>(T defaultValue)
        {
            return new VMPropBuilderFluent<T>(defaultValue);
        }

        public class VMPropBuilderFluent<T>
        {
            private readonly List<VMProp<T>.ValidationFunction<T>> _validators;
            private readonly T _defaultValue;
            private Action _onUserEdited;

            public VMPropBuilderFluent(T defaultValue)
            {
                _defaultValue = defaultValue;
                _validators = new List<VMProp<T>.ValidationFunction<T>>();
            }

            public VMPropBuilderFluent<T> AddOnUserChangeCallback(Action onUserEdited)
            {
                _onUserEdited = onUserEdited;
                return this;
            }

            public VMPropBuilderFluent<T> AddValidation(Func<T, bool> isValid, string errorDescription)
            {
                _validators.Add(new VMProp<T>.ValidationFunction<T>(errorDescription, isValid));
                return this;
            }

            public VMPropBuilderFluent<T> AddValidation(Func<T, bool> arePrereqsValid,
                                                        Func<T, bool> isValid,
                                                        string errorDescription)
            {
                _validators.Add(new VMProp<T>.ValidationFunction<T>(errorDescription, arePrereqsValid, isValid));
                return this;
            }

            public VMProp<T> Build()
            {
                return new VMProp<T>(_onUserEdited, _validators, _defaultValue);
            }
        }
    }
}