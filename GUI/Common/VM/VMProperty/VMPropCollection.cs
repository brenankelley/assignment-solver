// <copyright file="VMPropCollection.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.Common.VM.VMProperty
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Linq;

    public class VMPropCollection<T> : VMBase
    {
        private readonly Action _onUserEdited;
        private ObservableCollection<T> _internal;

        public VMPropCollection()
            : this(null, Enumerable.Empty<T>())
        {
        }

        public VMPropCollection(Action onUserEdited)
            : this(onUserEdited, Enumerable.Empty<T>())
        {
        }

        public VMPropCollection(IEnumerable<T> defaultValue)
            : this(null, defaultValue)
        {
        }

        public VMPropCollection(Action onUserEdited, IEnumerable<T> defaultValue)
        {
            _onUserEdited = onUserEdited;
            _internal = new ObservableCollection<T>(defaultValue);
        }

        public ObservableCollection<T> View => Internal;

        public IEnumerable<T> Model
        {
            get { return Internal; }
            set
            {
                Internal = new ObservableCollection<T>(value);
                OnPropertyChanged(nameof(View));
            }
        }

        private ObservableCollection<T> Internal
        {
            get { return _internal; }
            set
            {
                _internal.CollectionChanged -= OnCollectionChanged;
                _internal = value;
                _internal.CollectionChanged += OnCollectionChanged;
            }
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _onUserEdited?.Invoke();
        }
    }
}