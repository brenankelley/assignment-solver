// <copyright file="VMPreference.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.PreferenceView
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using Common.VM;
    using Common.VM.VMProperty;
    using Controller;
    using Data.Domain;
    using GongSolutions.Wpf.DragDrop;
    using Provider;
    using SubVMs;

    public class VMPreference : DropHandler, IVMPreference, IView
    {
        private readonly IWorkerProvider _workerProvider;
        private readonly IWorkerPreferenceProvider _preferenceProvider;
        private readonly ITaskProvider _taskProvider;
        private readonly VMPreferenceItemFactory _preferenceItemFactory;
        private readonly ISelectionProvider _selectionProvider;
       private readonly PreferenceController _preferenceController;

       public VMPreference(IWorkerProvider workerProvider,
                            IWorkerPreferenceProvider preferenceProvider,
                            ITaskProvider taskProvider,
                            VMPreferenceItemFactory preferenceItemFactory,
                            ISelectionProvider selectionProvider,
                            PreferenceController preferenceController)
           : base(false)
        {
            _workerProvider = workerProvider;
            _preferenceProvider = preferenceProvider;
            _taskProvider = taskProvider;
            _preferenceItemFactory = preferenceItemFactory;
            _selectionProvider = selectionProvider;
          _preferenceController = preferenceController;
          WorkerName = VMPropBuilder.Create(string.Empty)
                                      .Build();

            VMPreferenceItems = new VMPropCollection<IVMPreferenceItem>(new List<IVMPreferenceItem>());
            VMNoPreferenceItems = new VMPropCollection<IVMPreferenceItem>(new List<IVMPreferenceItem>());
        }

        public VMProp<string> WorkerName { get; }

        public VMPropCollection<IVMPreferenceItem> VMPreferenceItems { get; set; }

        public VMPropCollection<IVMPreferenceItem> VMNoPreferenceItems { get; set; }

        public override void Drop(IDropInfo dropInfo)
        {
            base.Drop(dropInfo);
            UserUpdate();
        }

        private void UserUpdate()
        {
            var worker = _selectionProvider.Workers.First();
            var taskPreferences = BuildTaskPreferences();
            _preferenceController.AddOrUpdatePreference(worker, taskPreferences);
        }

        public void Update()
        {
            var worker = _selectionProvider.Workers.FirstOrDefault();
            if (worker == null)
            {
                WorkerName.Model = string.Empty;
                VMPreferenceItems.Model = Enumerable.Empty<IVMPreferenceItem>();
                VMNoPreferenceItems.Model = Enumerable.Empty<IVMPreferenceItem>();
            }
            else
            {
                WorkerName.Model = _workerProvider.Workers.Single(w => w.Id.Equals(worker)).Name;
                VMPreferenceItems.Model = BuildVMPreferenceItems(worker);
                VMNoPreferenceItems.Model = BuildVMNoPreferenceItems(worker);
            }
        }

        private IEnumerable<IVMPreferenceItem> BuildVMPreferenceItems(Worker.WorkerId worker)
        {
            var taskPreference = WorkerPreference.ContainsKey(worker)
                                     ? WorkerPreference[worker].TaskPreferenceOrder
                                     : Enumerable.Empty<TaskPreferenceGroup>();

            foreach (var taskGroup in taskPreference)
            {
                var tasksInGroup = taskGroup.Tasks.OrderBy(i => i.GetHashCode()).ToList();
                foreach (var taskId in tasksInGroup)
                {
                    var isPinned = tasksInGroup.Count == 1;
                    yield return _preferenceItemFactory.Create(isPinned, taskId, UserUpdate);
                }
            }
        }

        private IEnumerable<IVMPreferenceItem> BuildVMNoPreferenceItems(Worker.WorkerId worker)
        {
            var taskPreference = WorkerPreference.ContainsKey(worker)
                                     ? WorkerPreference[worker].TaskPreferenceOrder
                                     : Enumerable.Empty<TaskPreferenceGroup>();
            var tasksWithPreference = taskPreference.SelectMany(t => t.Tasks)
                                                    .ToImmutableHashSet();

            var tasksWithoutPreference = Tasks.Select(t => t.Id)
                                              .Except(tasksWithPreference)
                                              .OrderBy(i => i.GetHashCode());

            foreach (var taskId in tasksWithoutPreference)
                yield return _preferenceItemFactory.Create(false, taskId, UserUpdate);
        }

        private TaskPreference BuildTaskPreferences()
        {
            var preferenceGroups = new List<TaskPreferenceGroup>();
            var consecutiveWithoutPreference = new List<Task.TaskId>();
            foreach (var vm in VMPreferenceItems.Model)
            {
                if (vm.Pinned.Model)
                {
                    if (consecutiveWithoutPreference.Any())
                    {
                        preferenceGroups.Add(new TaskPreferenceGroup(consecutiveWithoutPreference));
                        consecutiveWithoutPreference.Clear();
                    }

                    preferenceGroups.Add(new TaskPreferenceGroup(new[] { vm.Id }));
                }
                else
                {
                    consecutiveWithoutPreference.Add(vm.Id);
                }
            }

            if (consecutiveWithoutPreference.Any())
                preferenceGroups.Add(new TaskPreferenceGroup(consecutiveWithoutPreference));

            return new TaskPreference(preferenceGroups);
        }

        private IImmutableDictionary<Worker.WorkerId, TaskPreference> WorkerPreference => _preferenceProvider.WorkerPreference;

        private IImmutableSet<Task> Tasks => _taskProvider.Tasks;
    }
}
