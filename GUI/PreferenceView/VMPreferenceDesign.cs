// <copyright file="VMPreferenceDesign.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.GUI.PreferenceView
{
    using System.Collections.Generic;
    using AssignmentSolver.Data.Domain;
    using Common.VM.VMProperty;
    using SubVMs;

    public class VMPreferenceDesign : IVMPreference
    {
        public VMPreferenceDesign()
        {
            WorkerName = new VMProp<string>("Hard Worker 67");
            var preferenceItems = new[]
                                  {
                                      new VMPreferenceItem(true, "I'm pinned"),
                                      new VMPreferenceItem(false, "unpinned"),
                                      new VMPreferenceItem(false, "another task 1"),
                                      new VMPreferenceItem(false, "another task 2"),
                                      new VMPreferenceItem(true, "another pinned item"),
                                      new VMPreferenceItem(false, "last item is wanted least"),
                                  };
            VMPreferenceItems = new VMPropCollection<IVMPreferenceItem>(preferenceItems);

            var noPreferenceItems = new[]
                                        {
                                            new VMPreferenceItem(false, "no preference specified"),
                                            new VMPreferenceItem(false, "no preference 2"),
                                            new VMPreferenceItem(false, "no preference 3"),
                                        };
            VMNoPreferenceItems = new VMPropCollection<IVMPreferenceItem>(noPreferenceItems);
        }

        public VMProp<string> WorkerName { get; }
        public VMPropCollection<IVMPreferenceItem> VMPreferenceItems { get; }
        public VMPropCollection<IVMPreferenceItem> VMNoPreferenceItems { get; }

        public class VMPreferenceItem : IVMPreferenceItem
        {
            public VMPreferenceItem(bool pinned, string name)
            {
                Pinned = new VMProp<bool>(pinned);
                TaskName = name;
            }

            public Task.TaskId Id { get; }
            public VMProp<bool> Pinned { get; }
            public string TaskName { get; }
        }
    }
}