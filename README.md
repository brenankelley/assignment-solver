# Assignment Solver #
[![Build status](https://ci.appveyor.com/api/projects/status/g6b4peg9o15w1a68/branch/master?svg=true)](https://ci.appveyor.com/project/brenankelley/assignment-solver/branch/master)
[![Coverage Status](https://coveralls.io/repos/bitbucket/brenankelley/assignment-solver/badge.svg?branch=master)](https://coveralls.io/bitbucket/brenankelley/assignment-solver?branch=master)

## Description ##

Do you ever have trouble assigning tasks to workers?  
Do you workers feel your assignments are unfair?  
Do you want an automated way to maximize happiness for all workers?!?

This application should solve your problems!

## Contributing ##
### Environment ###
* Visual Studio Extensions
    * EditorConfig
    * Resharper (Ideally)

### Creating a Project ###
* Include *Common.targets* at end of project file
* Add NuGet Packages
    * Stylecop
    * Resharper Annotations

#### Creating a Test Project ###
* Include *Common.targets* at end of project file
* Add NuGet Packages
    * Stylecop
    * Resharper Annotations
    * JUnit
    * Fluent Assertions