﻿// <copyright file="AssignmentSolverTest.cs" company="Brenan Kelley and Austin Kelley">
// The MIT License (MIT)
// Copyright (c) Brenan Kelley and Austin Kelley
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>

namespace AssignmentSolver.ServiceTest
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using Data.Domain;
    using Data.Domain.Constraints;
    using FluentAssertions;
    using NUnit.Framework;
    using Service.AssignmentSolver;

    [TestFixture]
    public class AssignmentSolverTest
    {
        private AssignmentSolver _assignmentSolver;

        [SetUp]
        public void Setup()
        {
            _assignmentSolver = new AssignmentSolver();
        }

        [Test]
        public void WhenCalculate_ThenReturnEmpty()
        {
            var solverInput = CreateSolverInput();

            var output = _assignmentSolver.Solve(solverInput);

            output.Should().BeOfType<SolverOutput>();
        }

        private static SolverInput CreateSolverInput()
        {
            var solverConstraintsInput = new SolverConstraintsInput(
                new HashSet<TasksNotAssignedToSameWorker>().ToImmutableHashSet(),
                new HashSet<TaskAssignedToSpecificWorkers>().ToImmutableHashSet());

            return new SolverInput(new HashSet<Worker>().ToImmutableHashSet(),
                                   new HashSet<Task>().ToImmutableHashSet(),
                                   new Dictionary<Worker, TaskPreference>().ToImmutableDictionary(),
                                   solverConstraintsInput);
        }
    }
}
